import React from 'react'
import { createPortal } from 'react-dom'

import { Card } from 'components'
import { useBooleanState } from 'hooks'

import './snackbar.scss'

const Snackbar = ({ visible, children }) => {
  const [isVisible, show, hide] = useBooleanState()

  return createPortal(
    <div className={`snackbar ${visible ? 'visible' : 'hidden'}`}>
      {children}
    </div>,
    document.body
  )
}

export default Snackbar
