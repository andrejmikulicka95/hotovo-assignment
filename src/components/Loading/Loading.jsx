import React from 'react'

import './loading.scss'

const Loading = () => (
  <div className="loading">
    <img src="/images/favicon.ico" alt="Hotovo" />
    <svg width="80px" height="80px">
      <circle cx="40" cy="40" r="26" fill="transparent" strokeWidth="8" />
    </svg>
  </div>
)

export default Loading
