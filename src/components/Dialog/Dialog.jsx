import React from 'react'
import { createPortal } from 'react-dom'

import { Card } from 'components'

import './dialog.scss'

const Dialog = ({ onClose, children, className = '' }) =>
  createPortal(
    <div className={`dialog ${className}`}>
      <div className="dialog__dimmer" onClick={onClose} />
      <Card className="dialog__content">
        <img src="/images/close.svg" alt="x" onClick={onClose} />
        {children}
      </Card>
    </div>,
    document.body
  )

export default Dialog
