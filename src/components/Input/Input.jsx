import React, { forwardRef } from 'react'

import './input.scss'

const Input = (
  { placeholder, value, onChange, onEnterPressed, onFocus },
  ref
) => {
  const handleEnterPressed = e => {
    e.keyCode === 13 && onEnterPressed()
  }

  return (
    <input
      className="input"
      placeholder={placeholder}
      value={value}
      onChange={e => onChange(e.target.value)}
      onKeyUp={handleEnterPressed}
      onFocus={onFocus}
    />
  )
}

export default forwardRef(Input)
