import React from 'react'

import './card.scss'

const Card = ({ children, className = '' }) => (
  <div className={`card ${className}`}>{children}</div>
)

export default Card
