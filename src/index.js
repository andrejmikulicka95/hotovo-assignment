import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Switch, Route, Redirect } from 'react-router-dom'
import { createBrowserHistory } from 'history'

import { Loading } from 'components'
import { EmployeeExplorer, Overview, NotFound } from 'views'

import 'styles/global.scss'

const history = createBrowserHistory()

const App = () => (
  <React.Suspense fallback={<Loading />}>
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={EmployeeExplorer} />
        <Route
          path="/overview/:name"
          exact
          render={props => (
            <Overview key={props.match.params.name} {...props} />
          )}
        />
        <Route path="/404" exact component={NotFound} />
        <Redirect from="*" to="/" />
      </Switch>
    </Router>
  </React.Suspense>
)

ReactDOM.render(<App />, document.getElementById('root'))
