export const debounce = (fn, time = 500) => {
  let timeout

  clearTimeout(timeout)
  timeout = setTimeout(fn, time)
}
