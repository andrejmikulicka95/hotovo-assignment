export const callApi = path =>
  fetch(`http://api.additivasia.io/api/v1/assignment/employees/${path}`).then(
    res => res.json()
  )
