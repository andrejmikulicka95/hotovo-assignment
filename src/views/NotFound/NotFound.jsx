import React from 'react'

import { Button } from 'components'

import './notFound.scss'

const NotFound = ({ history }) => (
  <div className="notFound">
    <h1>Employee not found</h1>
    <Button onClick={() => history.push('/')}>Back to search</Button>
  </div>
)

export default NotFound
