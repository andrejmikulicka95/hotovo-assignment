import React, { useState } from 'react'

import { Card, Input, Button } from 'components'

import './employeeExplorer.scss'

const EmployeeExplorer = ({ history }) => {
  const [searchQuery, setSearchQuery] = useState('')

  const handleSearch = () => {
    searchQuery && history.push(`/overview/${searchQuery}`)
  }

  return (
    <div className="employeeExplorer">
      <h1>Employee Explorer</h1>
      <Card>
        <div className="employeeExplorer__form">
          <Input
            placeholder="Search..."
            value={searchQuery}
            onChange={setSearchQuery}
            onEnterPressed={handleSearch}
          />
          <Button onClick={handleSearch}>Search</Button>
        </div>
      </Card>
    </div>
  )
}

export default EmployeeExplorer
