import React, { useState, useEffect } from 'react'

import { callApi } from 'api'
import { Loading, Card, Button, Dialog, Input, Snackbar } from 'components'
import { useBooleanState } from 'hooks'
import { debounce } from 'utils'

import './overview.scss'

const Overview = ({ match, history }) => {
  const [loading, showLoading, hideLoading] = useBooleanState(true)
  const [data, setData] = useState('')
  const [indirectSubordinates, setIndirectSubordinates] = useState([])
  const [
    shareDialogVisible,
    showShareDialog,
    hideShareDialog
  ] = useBooleanState()
  const [snackbarVisible, showSnackbar, hideSnackbar] = useBooleanState()

  const recursiveApiCall = name => {
    callApi(name).then(res => {
      const subordinates = res[1] && res[1]['direct-subordinates']

      if (subordinates?.length) {
        subordinates.forEach(item => {
          setIndirectSubordinates(prevState => [
            ...new Set([...prevState, item])
          ])
          recursiveApiCall(item)
        })
      }
    })
  }

  useEffect(() => {
    showLoading()
    setIndirectSubordinates([])
    callApi(match.params.name).then(res => {
      if (res.length) {
        const resFormatted = {
          position: res[0],
          directSubordinates: res[1] && res[1]['direct-subordinates']
        }

        setData(resFormatted)

        resFormatted.directSubordinates?.forEach(item => recursiveApiCall(item))
      } else {
        history.push('/404')
      }
    })
  }, [])

  useEffect(() => {
    debounce(hideLoading, 1000)
  }, [indirectSubordinates])

  const copyAddress = e => {
    e.target.select()
    document.execCommand('copy')
    showSnackbar()

    setTimeout(hideSnackbar, 3000)
  }

  return (
    <div className="overview">
      {loading ? (
        <Loading />
      ) : (
        <>
          <div className="overview__actions">
            <button onClick={() => history.push('/')}>
              <img src="/images/search.svg" alt="Search" />
              Search
            </button>
            <button onClick={showShareDialog}>
              <img src="/images/share.svg" alt="Share" />
              Share
            </button>
          </div>
          <h1>{match.params.name}</h1>
          {data?.position && <h2>{data.position}</h2>}
          {data?.directSubordinates?.length || !!indirectSubordinates.length ? (
            <>
              {!!data.directSubordinates?.length && (
                <>
                  <div className="overview__subtitle">Direct Subordinates</div>
                  <Card className="overview__employees">
                    {data.directSubordinates.sort().map((item, idx) => (
                      <div
                        key={`direct-subordinates-item-${idx}`}
                        className="overview__employees__item"
                        onClick={() => history.push(`/overview/${item}`)}
                      >
                        {item}
                      </div>
                    ))}
                  </Card>
                </>
              )}
              {!!indirectSubordinates.length && (
                <>
                  <div className="overview__subtitle">
                    Indirect Subordinates
                  </div>
                  <Card className="overview__employees">
                    {indirectSubordinates.sort().map((item, idx) => (
                      <div
                        key={`indirect-subordinates-item-${idx}`}
                        className="overview__employees__item"
                        onClick={() => history.push(`/overview/${item}`)}
                      >
                        {item}
                      </div>
                    ))}
                  </Card>
                </>
              )}
            </>
          ) : (
            <>
              <div className="overview__subtitle">No subordinates</div>
              <Button onClick={() => history.push('/')}>Back To Search</Button>
            </>
          )}
        </>
      )}

      {shareDialogVisible && (
        <Dialog onClose={hideShareDialog}>
          <div className="overview__share__title">
            Click into the input to copy the address.
          </div>
          <Input value={window.location} onFocus={copyAddress} />
        </Dialog>
      )}

      <Snackbar visible={snackbarVisible}>Copied to clipboard</Snackbar>
    </div>
  )
}

export default Overview
